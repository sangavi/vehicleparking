package com.android.vehicleparking.fragment;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.vehicleparking.Adapter.NearbyAdapter;
import com.android.vehicleparking.SelfParking.SelfPrefConnect;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.android.vehicleparking.BaseRetrofit.BaseRetrofit;
import com.android.vehicleparking.R;
import com.android.vehicleparking.api.ApiInterface;
import com.android.vehicleparking.app.App;
import com.android.vehicleparking.Model.FavoriteList;
import com.android.vehicleparking.Model.Nearby;
import com.android.vehicleparking.Model.NearbyLocationRespone;
import com.android.vehicleparking.Model.NearbyResponse;
import com.android.vehicleparking.Model.UnparkResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;


public class SelfMapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    @BindView(R.id.mapFrame)
    FrameLayout mapFrame;
    @BindView(R.id.rv_places)
    RecyclerView rvPlaces;
    ;
    Unbinder unbinder;

    @BindView(R.id.inner)
    CoordinatorLayout inner;
    private GoogleApiClient mGoogleApiClient;
    private Location mylocation;

    LinearLayout bottomSheet;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    private GoogleMap mMap;
    private LatLng latLang;
    private Marker mLoc;

    ApiInterface api;
    private ArrayList<LatLng> latlngs = new ArrayList<>();
    public Response<NearbyLocationRespone> nearByResponse;
    private BottomSheetDialog mBottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private ArrayList<NearbyResponse> placeDetails;
    NearbyAdapter adapter;
    private ArrayList<FavoriteList> favlist;
    private int noReq;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_self_map, null, false);
        bottomSheet = (LinearLayout) view.findViewById(R.id.bottomSheet);

        api = BaseRetrofit.getDefaultRetrofit().create(ApiInterface.class);
        initMap();
        buildGoogleApiClient();


        MoveScreenApi();

        intBottomSheet();

        Log.e("Nive ", "onCreateView: " + SelfPrefConnect.readString(getActivity(), "Register", ""));


        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void MoveScreenApi() {

        if (App.getInstance() != null) {
            if (App.getInstance().isOnline()) {

                Call<UnparkResponse> call = api.unPark(SelfPrefConnect.readString(getActivity(), "UserId", ""), SelfPrefConnect.readString(getActivity(), "Book_Id", ""));
                call.enqueue(new Callback<UnparkResponse>() {
                    @Override
                    public void onResponse(Call<UnparkResponse> call, Response<UnparkResponse> response) {
                        if (response.isSuccessful()) {
                            DetectRequest(response);
                        }
                    }

                    @Override
                    public void onFailure(Call<UnparkResponse> call, Throwable t) {

                    }
                });

            } else {

            }
        }
    }

    private void DetectRequest(Response<UnparkResponse> response) {

        if (response.body().getMessage().equals("No Request Found")) {

            noReq = 0;

        } else if (response.body().getMessage().equals("Request Pending")) {

            noReq = 1;
        }
    }

    public void pendingReq() {
        BaseRetrofit.snack_bar(inner, "UnPark Your Vechicle then Park Request Will be Created Done!!! ", Snackbar.LENGTH_LONG, getActivity());
//        Toast.makeText(getActivity(), "", Toast.LENGTH_LONG).show();
    }


    private void intBottomSheet() {

        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        bottomSheetBehavior.setPeekHeight(100);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


    }


    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            this.mGoogleApiClient.disconnect();
        }
    }


    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(getActivity(),
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            getMyLocation();
        }
    }

    private void getMyLocation() {
        Log.e("Nive", "getMyLocation: ");
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                Log.e("Nive", "getMyLocation:connected ");
                int permissionLocation = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(2000);
                    locationRequest.setFastestInterval(2000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(mGoogleApiClient, locationRequest, this);
                    PendingResult result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(mGoogleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback() {

                        @Override
                        public void onResult(@NonNull Result result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.

                                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling
                                        //    ActivityCompat#requestPermissions
                                        // here to request the missing permissions, and then overriding
                                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                        //                                          int[] grantResults)
                                        // to handle the case where the user grants the permission. See the documentation
                                        // for ActivityCompat#requestPermissions for more details.

                                        mylocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


                                        Log.e("Nive", "onResult: " + mylocation);
//                                        setMarker(mylocation);
                                        onLocationChanged(mylocation);

                                        return;
                                    }


                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(getActivity(),
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied. However, we have no way to fix the
                                    // settings so we won't show the dialog.
                                    //finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        checkPermissions();

        callnearbylocation();

    }

    private void callnearbylocation() {


        Log.e("Nive ", "callnearbylocation: ");


        Nearby nearby = new Nearby();

        if (mylocation != null) {
            nearby.setLatitude(mylocation.getLatitude());
            nearby.setLongitude(mylocation.getLongitude());
            nearby.setId(SelfPrefConnect.readString(getActivity(), "UserId", ""));


            if (App.getInstance().isOnline()) {
                Call<NearbyLocationRespone> call = api.nearby(nearby);
                call.enqueue(new Callback<NearbyLocationRespone>() {


                    @Override
                    public void onResponse(Call<NearbyLocationRespone> call, Response<NearbyLocationRespone> response) {
                        if (response.isSuccessful()) {
                            nearByResponse = response;
                            Log.e("Nive ", "onResponse: " + response);
                            setMarkers(response);
                            setRecyclerview(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<NearbyLocationRespone> call, Throwable t) {

                    }
                });

            } else {

            }

        } else {


        }


    }

    private void setRecyclerview(NearbyLocationRespone body) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvPlaces.setLayoutManager(layoutManager);

        placeDetails = body.getData();
        favlist = body.getFav_list();
        Log.e("ResponseArray", " : " + placeDetails);
        adapter = new NearbyAdapter(this, placeDetails, favlist, noReq);
        adapter.setHasStableIds(true);
        rvPlaces.setItemAnimator(null);
        rvPlaces.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    private void setMarkers(Response<NearbyLocationRespone> response) {

        latlngs.clear();


        for (int i = 0; i < response.body().getData().size(); i++) {

            latlngs.add(new LatLng(response.body().getData().get(i).getLat(), response.body().getData().get(i).getLng()));

        }

        Log.e("Nive ", "setMarkers: NearMyResponse" + latlngs.size());
        Log.e("Nive ", "getData: " + response.body().getData().size());

        try {
            for (int i = 0; i < latlngs.size(); i++) {


                MarkerOptions markerOptions = new MarkerOptions();
                Log.e(TAG, "onLocationChanged: " + latlngs);
                markerOptions.position(latlngs.get(i));
                markerOptions.title(response.body().getData().get(i).getAddress1());
                markerOptions.icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_near)));
                mLoc = mMap.addMarker(markerOptions);

                //            mMap.addMarker(new MarkerOptions().title(response.body().getData().get(i).getAddress1()).position(latlngs.get(i)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_near)));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Nive ", "onRequestPermissionsResult: " + requestCode);
        getMyLocation();
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.e("Nive ", "onLocationChanged: " + location.getLatitude() + "Longnitude " + location.getLongitude());
        if (location != null) {

            this.mylocation = location;

            LatLng CurlatLng = new LatLng(location.getLatitude(), location.getLongitude());

            if (mLoc != null) {
                mLoc.remove();
            }
            MarkerOptions markerOptions = new MarkerOptions();
            Log.e(TAG, "onLocationChanged: " + CurlatLng);
            markerOptions.position(CurlatLng);
            markerOptions.title("Current Position");
            markerOptions.icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_loc)));
            mLoc = mMap.addMarker(markerOptions);

            //move map camera
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(CurlatLng, 17));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10));

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.e("Nive ", "onMapReady: ");


        this.mMap = googleMap;
//        checkPermission();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.mMap.setMyLocationEnabled(false);
        this.mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if (nearByResponse != null) {
                    for (int i = 0; i < nearByResponse.body().getData().size(); i++) {
                        if (marker.getTitle().equals(nearByResponse.body().getData().get(i).getAddress1())) {
                            marker.showInfoWindow();
                        }
                    }

                }


                return true;
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
