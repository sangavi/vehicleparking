package com.android.vehicleparking.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;


import com.android.vehicleparking.BaseRetrofit.BaseRetrofit;
import com.android.vehicleparking.Model.UnparkResponse;
import com.android.vehicleparking.R;
import com.android.vehicleparking.SelfParking.SelfMainActivity;
import com.android.vehicleparking.SelfParking.SelfPrefConnect;
import com.android.vehicleparking.SelfParking.SelfProgressUtils;
import com.android.vehicleparking.api.ApiInterface;
import com.android.vehicleparking.app.App;
import com.balysv.materialripple.MaterialRippleLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfUnParkFragment extends Fragment {

    @BindView(R.id.tv_slotId)
    AppCompatTextView tvSlotId;
    @BindView(R.id.bt_unParkReq)
    AppCompatButton btUnParkReq;
    Unbinder unbinder;

    ApiInterface apiInterface;
    SelfProgressUtils selfProgressUtils;
    @BindView(R.id.parentLay)
    RelativeLayout parentLay;
    @BindView(R.id.bookingIdLay)
    LinearLayout bookingId;
    @BindView(R.id.ratinBar)
    RatingBar ratinBar;
    @BindView(R.id.unpark_ripple)
    MaterialRippleLayout policeRipple;
    private AlertDialog.Builder builder, builderSucess;
    private AlertDialog ad1, ad2;
    private Runnable runnable;
    private Handler handler = new Handler();
    private boolean enterFirst = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_self_un_park, null, false);
        unbinder = ButterKnife.bind(this, view);
//        tvSlotId.setText(PrefConnect.readString(getActivity(), "BookId", ""));
        tvSlotId.setText(SelfPrefConnect.readString(getActivity(), "SlotId", ""));
        apiInterface = BaseRetrofit.getDefaultRetrofit().create(ApiInterface.class);
        selfProgressUtils = new SelfProgressUtils(getActivity());

        if (tvSlotId.getText().toString().isEmpty()) {

            bookingId.setVisibility(View.GONE);

        } else {
            bookingId.setVisibility(View.VISIBLE);
        }
        return view;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.bt_unParkReq)
    public void onViewClicked() {


        if (tvSlotId.getText().toString().isEmpty()) {
            BaseRetrofit.snack_bar(parentLay, "Request Pending is Not Available!!! ", Snackbar.LENGTH_LONG, getActivity());
//            Toast.makeText(getActivity(), "Request Pending is Not Available", Toast.LENGTH_LONG).show();
        } else {
            runnable = new Runnable() {
                public void run() {
                    callapi();
                    handler.postDelayed(runnable, 1000);
                }
            };
            handler.postDelayed(runnable, 100);
        }


    }

    private void callapi() {

        if (App.getInstance().isOnline()) {
//            progressUtils.show();
            Call<UnparkResponse> call = apiInterface.unParkRequest(SelfPrefConnect.readString(getActivity(), "Book_Id", ""));
            call.enqueue(new Callback<UnparkResponse>() {
                @Override
                public void onResponse(Call<UnparkResponse> call, Response<UnparkResponse> response) {
//                    progressUtils.dismiss();
                    if (response.isSuccessful()) {

                        if (response.body().getMessage().equals("1")) {

                            if (enterFirst) {


                            } else {
                                callAlert();
                            }


                        } else if (response.body().getMessage().equals("2")) {

                            sendSucess();

                        }


                    }
                }

                @Override
                public void onFailure(Call<UnparkResponse> call, Throwable t) {
                    selfProgressUtils.dismiss();

                }
            });

        } else {

        }
    }

    private void sendSucess() {

        handler.removeCallbacks(runnable);

        builderSucess = new AlertDialog.Builder(getActivity());


        builderSucess.setTitle("UnPark");
        builderSucess.setMessage("Upark Request Apreoval Done");
        builderSucess.setCancelable(false);
        ad2 = builderSucess.create();

        if (!ad2.isShowing()) {
            ad2.show();
        }


        builderSucess.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                tvSlotId.setText("");
                SelfPrefConnect.writeString(getActivity(), "Book_Id", "");
                Intent intent = new Intent(getActivity(), SelfMainActivity.class);
                startActivity(intent);
                ad2.cancel();
                ad1.dismiss();


            }
        });


        AlertDialog alert = builderSucess.create();
        alert.show();

    }

    private void callAlert() {

        builder = new AlertDialog.Builder(getActivity());
        enterFirst = true;

        builder.setTitle("UnPark");
        builder.setMessage("Waiting For UnPark Request Approval");
        builder.setCancelable(false);
        ad1 = builder.create();

        if (!ad1.isShowing()) {
            ad1.show();
        }


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                enterFirst = false;
                ad1.cancel();


            }
        });


        AlertDialog alert = builder.create();
        alert.show();
    }
}
