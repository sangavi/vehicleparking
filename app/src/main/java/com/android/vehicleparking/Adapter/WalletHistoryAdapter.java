package com.android.vehicleparking.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.vehicleparking.Model.WalletHistoryPojo;
import com.android.vehicleparking.R;
import com.android.vehicleparking.WalletParking.WalletHistoryDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {


    Context context;
    ArrayList<WalletHistoryPojo> arraylist;

    public WalletHistoryAdapter(Context context, ArrayList<WalletHistoryPojo> arrayList) {
        this.context = context;
        arraylist = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_comment, parent, false);
        return new WalletHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.car_brand.setText((Html.fromHtml("Vehicle Brand : " + "<font color='#0000'>" + arraylist.get(position).getVehicle_brand()
                + " ( " + arraylist.get(position).getVehicle_color() + " )" + "</font>")));
        holder.car_number.setText((Html.fromHtml("Vehicle Number : " + "<font color='#0000'>" + arraylist.get(position).getVehicle_number() + "</font>")));
        holder.parking_date.setText((Html.fromHtml("Parking Date : " + "<font color='#0000'>" + arraylist.get(position).getParking_date()
                + " ( " + arraylist.get(position).getParking_time() + " )" + "</font>")));
//        holder.parking_time.setText((Html.fromHtml("Parking Time : " + "<font color='#0000'>" + arraylist.get(position).getParking_time() + "</font>")));

        holder.cardview_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("id", arraylist.get(position).getId());
                hashMap.put("lat", arraylist.get(position).getLat());
                hashMap.put("lang", arraylist.get(position).getLang());
                hashMap.put("vehicle_brand", arraylist.get(position).getVehicle_brand());
                hashMap.put("vehicle_number", arraylist.get(position).getVehicle_number());
                hashMap.put("vehicle_color", arraylist.get(position).getVehicle_color());
                hashMap.put("parking_date", arraylist.get(position).getParking_date());
                hashMap.put("parking_time", arraylist.get(position).getParking_time());
                hashMap.put("total_time", arraylist.get(position).getTotal_time());
                hashMap.put("amount", arraylist.get(position).getAmount());
                hashMap.put("status", arraylist.get(position).getStatus());
                hashMap.put("payment_status", arraylist.get(position).getPayment_status());
                hashMap.put("parker_name", arraylist.get(position).getParker_name());
                hashMap.put("parker_mobile", arraylist.get(position).getParker_mobile());

                Intent intent = new Intent(context, WalletHistoryDetailActivity.class);
                intent.putExtra("HistoryListExtra", hashMap);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView car_brand, car_number, parking_date, parking_time;
        CardView cardview_history;

        public ViewHolder(View itemView) {
            super(itemView);

            cardview_history = itemView.findViewById(R.id.cardview_history);
            car_brand = itemView.findViewById(R.id.car_brand);
            car_number = itemView.findViewById(R.id.car_number);
            parking_date = itemView.findViewById(R.id.parking_date);
            parking_time = itemView.findViewById(R.id.parking_time);
        }
    }
}
