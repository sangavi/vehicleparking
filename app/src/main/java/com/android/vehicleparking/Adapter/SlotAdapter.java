package com.android.vehicleparking.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.android.vehicleparking.R;
import com.android.vehicleparking.SelfParking.SelfGridSlotActivity;

import java.util.ArrayList;

public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.ViewHolder> {

    SelfGridSlotActivity selfGridSlotActivity;
    ArrayList<String> integerArrayList;
    ArrayList<String> filledList;
    private int pos;


    public SlotAdapter(SelfGridSlotActivity selfGridSlotActivity, ArrayList<String> slotList, ArrayList<String> filledList) {

        this.selfGridSlotActivity = selfGridSlotActivity;
        this.integerArrayList = slotList;
        this.filledList = filledList;

        Log.e("Nive ", "SlotAdapter: " + filledList.size());

    }

    @NonNull
    @Override
    public SlotAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.self_grid_item_slot, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SlotAdapter.ViewHolder holder, final int position) {


        if (filledList.size() > 0) {


            if (pos == position) {

                holder.linearLayout.setBackgroundColor(Color.BLUE);
            } else {

                Log.e("Nive ", "UnMatched: ");
                holder.linearLayout.setBackgroundColor(Color.GRAY);


            }


            if (filledList.size() > 0) {

                Log.e("Nive ", "FilledList: " + filledList.size() + " " + filledList.get(position));
                holder.tv_slot_number.setText(filledList.get(position));

                if (filledList.get(position).equals("0")) {
                    Log.e("Nive ", "Matched: ");
                    holder.tv_slot_number.setText("Parked");
                    holder.linearLayout.setBackgroundColor(Color.RED);

                } else {
                    Log.e("Nive ", "UnMatched: ");
                    holder.linearLayout.setBackgroundColor(Color.GRAY);

                }
            }


            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (filledList.size() > 0) {
                        if (filledList.get(position).equals("0")) {

                            holder.tv_slot_number.setText("Parked");
                            selfGridSlotActivity.selectedButton("0");


                        } else {
                            selfGridSlotActivity.selectedButton("");
                            selfGridSlotActivity.selectedId(filledList.get(position));


                        }
                    }

                }
            });


        }


    }


    public void OnItemClicked(int position) {
        Log.e("Nive ", "OnItemClicked: " + position);
        pos = position;
        //Log.d("mahi", "pos" + pos);
    }

    @Override
    public int getItemCount() {
        return filledList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        LinearLayout linearLayout;
        AppCompatTextView tv_slot_number;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardSlot);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear);
            tv_slot_number = (AppCompatTextView) itemView.findViewById(R.id.tv_slot_number);
        }
    }
}
