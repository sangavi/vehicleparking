package com.android.vehicleparking.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.vehicleparking.Model.FavoriteList;
import com.android.vehicleparking.Model.NearbyResponse;
import com.android.vehicleparking.R;
import com.android.vehicleparking.SelfParking.SelfGridSlotActivity;
import com.android.vehicleparking.fragment.SelfMapFragment;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NearbyAdapter extends RecyclerView.Adapter<NearbyAdapter.ViewHolder> {


    ArrayList<NearbyResponse> nearbyResponseArrayList = new ArrayList<>();
    SelfMapFragment selfMapFragment;
    ArrayList<FavoriteList> favoriteLists = new ArrayList<>();
    int req;

    public NearbyAdapter(SelfMapFragment selfMapFragment, ArrayList<NearbyResponse> placeDetails, ArrayList<FavoriteList> favlist, int noReq) {

        this.selfMapFragment = selfMapFragment;
        this.nearbyResponseArrayList = placeDetails;
        this.favoriteLists = favlist;
        this.req = noReq;

    }


    @NonNull
    @Override
    public NearbyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.self_listview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NearbyAdapter.ViewHolder holder, final int position) {

        if (nearbyResponseArrayList.size() > 0) {


            holder.city.setText(nearbyResponseArrayList.get(position).getAddress1());
            holder.address.setText(nearbyResponseArrayList.get(position).getAddress2());
            String dis = String.valueOf(nearbyResponseArrayList.get(position).getDistance());

            holder.distance.setText(dis + "KM");

          /*  if (favoriteLists.size() > 0) {

            } else {
                if (nearbyResponseArrayList.get(position).getId().contains(favoriteLists.get(position).getPlace_id())) {
                    if (favoriteLists.get(position).getIs_favourite().equals("1")) {

                        holder.imageView.setImageResource(R.drawable.red_heart);


                    } else {
                        holder.imageView.setImageResource(R.drawable.heart_black);
                    }

                }
            }*/


//            holder.distance.setText(nearbyResponseArrayList.get(position).getLng());


            //norequest found
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (req == 0) {
                        Intent intent = new Intent(selfMapFragment.getActivity(), SelfGridSlotActivity.class);
                        intent.putExtra("PlaceId", nearbyResponseArrayList.get(position).getId());
                        intent.putExtra("Address", nearbyResponseArrayList.get(position).getAddress1());
                        intent.putExtra("Fee", nearbyResponseArrayList.get(position).getFee());
                        selfMapFragment.startActivity(intent);
                    } else {

                        selfMapFragment.pendingReq();

                    }


                }
            });


        }

    }

    @Override
    public int getItemCount() {
        return nearbyResponseArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;
        ImageView imageView;
        AppCompatTextView city, distance, address, time;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.image);
            imageView = (ImageView) itemView.findViewById(R.id.heartImage);
            city = (AppCompatTextView) itemView.findViewById(R.id.city);
            distance = (AppCompatTextView) itemView.findViewById(R.id.km);
            address = (AppCompatTextView) itemView.findViewById(R.id.address);
            time = (AppCompatTextView) itemView.findViewById(R.id.openHrs);

        }
    }
}
