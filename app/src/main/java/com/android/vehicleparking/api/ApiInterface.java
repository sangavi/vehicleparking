package com.android.vehicleparking.api;



import com.android.vehicleparking.Model.AddCardRequest;
import com.android.vehicleparking.Model.AddCardResponse;
import com.android.vehicleparking.Model.AvailableSlotsResponse;
import com.android.vehicleparking.Model.BookSlotRequest;
import com.android.vehicleparking.Model.BookSlotResponse;
import com.android.vehicleparking.Model.CashRequest;
import com.android.vehicleparking.Model.CashResponse;
import com.android.vehicleparking.Model.Nearby;
import com.android.vehicleparking.Model.NearbyLocationRespone;
import com.android.vehicleparking.Model.RegisterResponse;
import com.android.vehicleparking.Model.UnparkResponse;
import com.android.vehicleparking.Model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {


    @POST("Register")
    Call<RegisterResponse> callLogin(@Body User user);


    @POST("get_nearby")
    Call<NearbyLocationRespone> nearby(@Body Nearby nearby);


    @GET("get_booked_slots")
    Call<AvailableSlotsResponse> getAvailable(@Query("place_id") String id);

    @POST("book_slot")
    Call<BookSlotResponse> bookSlot(@Body BookSlotRequest bookSlotRequest);

    @POST("track_payment")
    Call<CashResponse> getStatus(@Body CashRequest cashRequest);

    @GET("unpark_request")
    Call<UnparkResponse> unParkRequest(@Query("unpark_id") String unparkId);

    @GET("request_check")
    Call<UnparkResponse> unPark(@Query("user_id") String unparkId, @Query("park_id") String park_id);

    @POST("add_card_paystack")
    Call<AddCardResponse> addCard(@Body AddCardRequest addCardRequest);

//    @POST("payment)


}
