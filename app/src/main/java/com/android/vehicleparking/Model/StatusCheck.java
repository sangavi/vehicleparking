package com.android.vehicleparking.Model;

public class StatusCheck {

    public String parking_status;

    public String getParking_status() {
        return parking_status;
    }

    public void setParking_status(String parking_status) {
        this.parking_status = parking_status;
    }
}
