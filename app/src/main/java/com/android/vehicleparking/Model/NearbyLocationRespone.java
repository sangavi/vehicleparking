package com.android.vehicleparking.Model;

import java.util.ArrayList;

public class NearbyLocationRespone {

    public ArrayList<NearbyResponse> data;

    public ArrayList<NearbyResponse> getData() {
        return data;
    }


    public ArrayList<FavoriteList> fav_list;

    public ArrayList<FavoriteList> getFav_list() {
        return fav_list;
    }

    public void setFav_list(ArrayList<FavoriteList> fav_list) {
        this.fav_list = fav_list;
    }

    public void setData(ArrayList<NearbyResponse> data) {
        this.data = data;
    }
}
