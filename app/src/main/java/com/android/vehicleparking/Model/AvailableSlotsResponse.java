package com.android.vehicleparking.Model;

import java.util.ArrayList;

public class AvailableSlotsResponse {

    public ArrayList<AvailableSlots> data;

    public ArrayList<AvailableSlots> getData() {
        return data;
    }

    public void setData(ArrayList<AvailableSlots> data) {
        this.data = data;
    }
}
