package com.android.vehicleparking.Model;

public class AddCardRequest {

    public String month;
    public String year;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCard_nbr() {
        return card_nbr;
    }

    public void setCard_nbr(String card_nbr) {
        this.card_nbr = card_nbr;
    }

    public String cvv;
    public String card_nbr;
}
