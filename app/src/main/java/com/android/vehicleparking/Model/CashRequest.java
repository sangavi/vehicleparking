package com.android.vehicleparking.Model;

public class CashRequest {

    public String book_id;
    public String park_in;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String date;

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getPark_in() {
        return park_in;
    }

    public void setPark_in(String park_in) {
        this.park_in = park_in;
    }

    public String getPark_out() {
        return park_out;
    }

    public void setPark_out(String park_out) {
        this.park_out = park_out;
    }

    public String park_out;
}
