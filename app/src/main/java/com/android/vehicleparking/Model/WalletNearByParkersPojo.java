package com.android.vehicleparking.Model;

import com.google.android.gms.maps.model.LatLng;

public class WalletNearByParkersPojo {

    private String id;
    private LatLng latlan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LatLng getLatlan() {
        return latlan;
    }

    public void setLatlan(LatLng latlan) {
        this.latlan = latlan;
    }
}
