package com.android.vehicleparking.Model;

import java.io.Serializable;

public class BookSlotResponse implements Serializable {

    /*public BookingDetails getData() {
        return data;
    }

    public void setData(BookingDetails data) {
        this.data = data;
    }

    public BookingDetails  data;*/

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(String slot_id) {
        this.slot_id = slot_id;
    }

    public String id;
    public String amount;
    public String slot_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
