package com.android.vehicleparking.Fcm;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String keyTitle;
    PendingIntent pIntent;

    Intent intent = null;

    public int notificationCount = 0;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (remoteMessage.getNotification() != null) {

            Log.e(TAG, "getNotification: " + remoteMessage.getFrom());
            Log.e(TAG, "getMessageId: " + remoteMessage.getMessageId());
            Log.e(TAG, "getTo: " + remoteMessage.getTo());
            Log.e(TAG, "getTtl: " + remoteMessage.getTtl());
            Log.e(TAG, "getTitle: " + remoteMessage.getNotification().getTitle());
            Log.e(TAG, "getTitleLocalizationArgs: " + remoteMessage.getNotification().getTitleLocalizationArgs());

            Log.e(TAG, "remoteMessage.getTag(): " + remoteMessage.getNotification().getTag());

            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            //setNotification();
        }


    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String title) {

    }

}