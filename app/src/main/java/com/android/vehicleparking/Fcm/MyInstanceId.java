package com.android.vehicleparking.Fcm;

import android.util.Log;

import com.android.vehicleparking.Common.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Admin on 2/27/2018.
 */

public class MyInstanceId extends FirebaseInstanceIdService {
    private static final String TAG = "MyInstanceId";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        sessionManager.saveDeviceToken(refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }
}

