package com.android.vehicleparking.SelfParking;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.android.vehicleparking.BaseRetrofit.BaseRetrofit;
import com.android.vehicleparking.R;
import com.android.vehicleparking.api.ApiInterface;
import com.android.vehicleparking.app.App;
import com.android.vehicleparking.Model.BookSlotResponse;
import com.android.vehicleparking.Model.CashRequest;
import com.android.vehicleparking.Model.CashResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfPayActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLay)
    AppBarLayout appBarLay;
    @BindView(R.id.textLay)
    AppCompatTextView textLay;
    @BindView(R.id.currentTime)
    AppCompatTextView currentTime;
    @BindView(R.id.currentLay)
    LinearLayout currentLay;
    @BindView(R.id.selectedTime)
    AppCompatTextView selectedTime;
    @BindView(R.id.selectedLay)
    LinearLayout selectedLay;
    @BindView(R.id.paymentOption)
    LinearLayout paymentOption;
    @BindView(R.id.cash)
    AppCompatTextView cash;
    @BindView(R.id.netBanking)
    AppCompatTextView netBanking;

    AlertDialog ad, ad1;


    Runnable runnable;
    Handler handler = new Handler();
    @BindView(R.id.amount)
    AppCompatTextView amount;
    @BindView(R.id.parentLay)
    RelativeLayout parentLay;
    private BookSlotResponse response;

    ApiInterface apiInterface;
    String localTime, localDate;

    private int mYear, mMonth, mDay, mHour, mMinute, mSeconds;
    public int totalTime;

    SelfProgressUtils selfProgressUtils;
    AlertDialog.Builder builder;
    private boolean firseEnter = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_pay2);
        ButterKnife.bind(this);

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("MMM dd,yyyy");
        DateFormat time = new SimpleDateFormat("hh:mm a");
        date.setTimeZone(TimeZone.getDefault());
        time.setTimeZone(TimeZone.getDefault());
        selfProgressUtils = new SelfProgressUtils(SelfPayActivity.this);
        localTime = time.format(currentLocalTime);
        localDate = date.format(currentLocalTime);

//        setToolbar();
        apiInterface = BaseRetrofit.getDefaultRetrofit().create(ApiInterface.class);

        if (getIntent() != null) {
            Intent intent = getIntent();
            response = (BookSlotResponse) intent.getSerializableExtra("BookConfirm");

        }

        if (response != null) {

            amount.setText("Your Amount is " + response.getAmount());
            textLay.setText("Your Slot Number is " + response.getSlot_id());
            SelfPrefConnect.writeString(SelfPayActivity.this, "SlotId", response.getSlot_id());


        }


        Log.e("Nive ", "onCreateView: " + localTime);

        currentTime.setText(localTime);
    }

    public void setToolbar() {
        toolbar.setNavigationIcon(R.drawable.back);


        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
    }

    @OnClick({R.id.selectedTime, R.id.cash, R.id.netBanking})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.selectedTime:
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                mHour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                mMinute = mcurrentTime.get(Calendar.MINUTE);
                mSeconds = mcurrentTime.get(Calendar.SECOND);
                mYear = mcurrentTime.get(Calendar.YEAR);
                mDay = mcurrentTime.get(Calendar.DAY_OF_MONTH);
                mMonth = mcurrentTime.get(Calendar.MONTH);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(SelfPayActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        selectedTime.setText(selectedHour + ":" + selectedMinute);

                    }
                }, mHour, mMinute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();


                break;
            case R.id.cash:
                if (selectedTime.getText().toString().isEmpty()) {
                    BaseRetrofit.snack_bar(parentLay, "Please Select End Time!!! ", Snackbar.LENGTH_LONG, SelfPayActivity.this);
//                    Toast.makeText(PayActivity.this, "Please Select End Time", Toast.LENGTH_LONG).show();
                } else {
                    callApi();
                }


//                callDialog();
                break;
            case R.id.netBanking:

                break;
        }
    }

    private void callApi() {

        runnable = new Runnable() {
            public void run() {
                callCashApi();
                handler.postDelayed(runnable, 1000);
            }
        };
        handler.postDelayed(runnable, 100);


    }

    private void callCashApi() {

        totalTime = mHour + mMinute + mSeconds + mYear + mMonth + mDay;

        SelfPrefConnect.writeString(SelfPayActivity.this, "Book_Id", response.getId());

        CashRequest cashRequest = new CashRequest();
        cashRequest.setBook_id(response.getId());
        cashRequest.setPark_in(localTime);
        cashRequest.setDate(localDate);
        cashRequest.setPark_out(String.valueOf(totalTime));

        if (App.getInstance().isOnline()) {
//            progressUtils.show();
            Call<CashResponse> call = apiInterface.getStatus(cashRequest);
            call.enqueue(new Callback<CashResponse>() {
                @Override
                public void onResponse(Call<CashResponse> call, Response<CashResponse> response) {
                    if (response.isSuccessful()) {
//                        progressUtils.dismiss();

                        if (response.body().getData().getParking_status().equals("1")) {

                            if (firseEnter) {

                            } else {
                                showAlert(response);
                            }

                        } else {
                            if (response.body().getData().getParking_status().equals("2")) {
                                callDialog();
                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<CashResponse> call, Throwable t) {
                    selfProgressUtils.dismiss();
                }
            });

        } else {

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void showAlert(Response<CashResponse> response) {

        firseEnter = true;
        builder = new AlertDialog.Builder(this);

        builder.setTitle("Approval");
        builder.setMessage("Waiting For Payment Aproval ");
        builder.setCancelable(false);
        ad1 = builder.create();

        if (!ad1.isShowing()) {
            ad1.show();
        }


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                firseEnter = false;
                ad1.cancel();


            }
        });


        AlertDialog alert = builder.create();
        alert.show();
    }

    private void callDialog() {

        handler.removeCallbacks(runnable);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);


        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.self_dialog_payment_sucessfull, null);
        dialog.setView(dialogView);
        AppCompatTextView ok = (AppCompatTextView) dialogView.findViewById(R.id.ok);
        LinearLayout linearLayout = (LinearLayout) dialogView.findViewById(R.id.linear);
        dialog.setCancelable(false);
        ad = dialog.create();


        if (!ad.isShowing()) {
            ad.show();
        }


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.cancel();
                ad1.cancel();
                ad1.dismiss();

                Intent intent = new Intent(SelfPayActivity.this, SelfMainActivity.class);
                startActivity(intent);
            }
        });

    }
}
