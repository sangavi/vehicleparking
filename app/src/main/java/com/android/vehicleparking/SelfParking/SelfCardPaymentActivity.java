package com.android.vehicleparking.SelfParking;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.vehicleparking.BaseRetrofit.BaseRetrofit;
import com.android.vehicleparking.R;
import com.android.vehicleparking.api.ApiInterface;
import com.android.vehicleparking.app.App;
import com.android.vehicleparking.Model.AddCardRequest;
import com.android.vehicleparking.Model.AddCardResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfCardPaymentActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLay)
    AppBarLayout appBarLay;
    @BindView(R.id.close)
    FrameLayout close;
    @BindView(R.id.cardNbr)
    AppCompatEditText cardNbr;
    @BindView(R.id.month)
    AppCompatEditText month;
    @BindView(R.id.year)
    AppCompatEditText year;
    @BindView(R.id.cvv)
    AppCompatEditText cvv;
    @BindView(R.id.addCard)
    AppCompatButton addCard;
    @BindView(R.id.pay)
    AppCompatButton pay;

    SelfProgressUtils selfProgressUtils;

    ApiInterface apiInterface;
    @BindView(R.id.parentLay)
    LinearLayout parentLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.self_dialog_send_details);
        ButterKnife.bind(this);
        setToolbar();
        selfProgressUtils = new SelfProgressUtils(this);
        apiInterface = BaseRetrofit.getDefaultRetrofit().create(ApiInterface.class);

    }

    public void setToolbar() {
        toolbar.setNavigationIcon(R.drawable.back);


        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
    }

    @OnClick({R.id.addCard, R.id.pay})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addCard:
                if (cardNbr.getText().toString().isEmpty()) {
                    BaseRetrofit.snack_bar(parentLay, "Please Enter Card Number!!! ", Snackbar.LENGTH_LONG, SelfCardPaymentActivity.this);
                } else if (cvv.getText().toString().isEmpty()) {
                    BaseRetrofit.snack_bar(parentLay, "Please Enter CVV!!! ", Snackbar.LENGTH_LONG, SelfCardPaymentActivity.this);
                } else if (year.getText().toString().isEmpty()) {
                    BaseRetrofit.snack_bar(parentLay, "Please Enter Year!!! ", Snackbar.LENGTH_LONG, SelfCardPaymentActivity.this);
                } else if (month.getText().toString().isEmpty()) {
                    BaseRetrofit.snack_bar(parentLay, "Please Enter Month!!! ", Snackbar.LENGTH_LONG, SelfCardPaymentActivity.this);
                } else {
                    callCardAdded();
                }

                break;
            case R.id.pay:

                sendPayment();
                break;
        }
    }

    private void sendPayment() {
    }

    private void callCardAdded() {

        AddCardRequest addCardRequest = new AddCardRequest();
        addCardRequest.setCard_nbr(cardNbr.getText().toString());
        addCardRequest.setCvv(cvv.getText().toString());
        addCardRequest.setMonth(month.getText().toString());
        addCardRequest.setYear(year.getText().toString());

        if (App.getInstance().isOnline()) {
            selfProgressUtils.show();
            Call<AddCardResponse> call = apiInterface.addCard(addCardRequest);
            call.enqueue(new Callback<AddCardResponse>() {
                @Override
                public void onResponse(Call<AddCardResponse> call, Response<AddCardResponse> response) {
                    selfProgressUtils.dismiss();
                    if (response.isSuccessful()) {
                        BaseRetrofit.snack_bar(parentLay, "Card!!! ", Snackbar.LENGTH_LONG, SelfCardPaymentActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<AddCardResponse> call, Throwable t) {
                    selfProgressUtils.dismiss();

                }
            });

        } else {

        }


    }
}
