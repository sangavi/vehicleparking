package com.android.vehicleparking.SelfParking;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.android.vehicleparking.R;
import com.android.vehicleparking.fragment.SelfMapFragment;
import com.android.vehicleparking.fragment.SelfUnParkFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelfMainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer)
    DrawerLayout drawer;

    FragmentManager fragmentManager;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    SelfUnParkFragment unParkFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_main);
        ButterKnife.bind(this);


        initNavigationDrawer();

        //   addDivederLine navigation menu

        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(SelfMainActivity.this, DividerItemDecoration.VERTICAL));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void initNavigationDrawer() {


        fragmentManager = getSupportFragmentManager();


        SelfMapFragment menu = new SelfMapFragment();
        fragmentManager.beginTransaction().replace(R.id.frame, menu).commit();


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();


                switch (id) {
                    case R.id.homeMap:
                        SelfMapFragment menu = new SelfMapFragment();
                        fragmentManager.beginTransaction().replace(R.id.frame, menu).commit();
                        drawer.closeDrawers();
                        break;

                    case R.id.fav:
                        unParkFragment = new SelfUnParkFragment();
                        fragmentManager.beginTransaction().replace(R.id.frame, unParkFragment).commit();
                        drawer.closeDrawers();
                        break;

                    case R.id.settings:
//                        RatingsFragment ratingsFragment = new RatingsFragment();
//                        fragmentManager.beginTransaction().replace(R.id.frame, ratingsFragment).commit();
                        drawer.closeDrawers();
                        break;


                    case R.id.payment:
//
//                        MenuListActivity menu = new MenuListActivity();
//                        fragmentManager.beginTransaction().replace(R.id.frame, menu).commit();
                        drawer.closeDrawers();

                        break;

                    case R.id.feedback:

//                        presenter.logout();

                        break;

                    case R.id.help:

//                        MenuListActivity menu = new MenuListActivity();
//                        fragmentManager.beginTransaction().replace(R.id.frame, menu).commit();
                        drawer.closeDrawers();

                        break;

                    case R.id.about_us:

//                        MenuListActivity menu = new MenuListActivity();
//                        fragmentManager.beginTransaction().replace(R.id.frame, menu).commit();
                        drawer.closeDrawers();

                        break;

                }
                return true;
            }
        });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {


            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


    }

}
