package com.android.vehicleparking.SelfParking;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.android.vehicleparking.R;


public class SelfProgressUtils {

    private static Dialog mDialog;
//    private ProgressDialog progressDialog;
//    private String defaultMsg;

    public SelfProgressUtils(Context context) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setCancelable(false);
//        progressDialog.setIndeterminate(true);
//        defaultMsg = context.getString(R.string.loading);
//        progressDialog.setMessage(defaultMsg);

        if (context != null) {
            mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setCancelable(false);
            mDialog.setContentView(R.layout.self_animation_loading);
        }

    }


    public void show() {
        try {
            if (mDialog != null && !mDialog.isShowing()) {
                mDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismiss() {
        try {
            if (mDialog != null) {
                mDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
