package com.android.vehicleparking.SelfParking;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.vehicleparking.R;
import com.android.vehicleparking.BaseRetrofit.BaseRetrofit;
import com.android.vehicleparking.Adapter.SlotAdapter;
import com.android.vehicleparking.api.ApiInterface;
import com.android.vehicleparking.app.App;
import com.android.vehicleparking.Model.AvailableSlots;
import com.android.vehicleparking.Model.AvailableSlotsResponse;
import com.android.vehicleparking.Model.BookSlotRequest;
import com.android.vehicleparking.Model.BookSlotResponse;
import com.android.vehicleparking.Model.ItemClickSupport;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfGridSlotActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLay)
    AppBarLayout appBarLay;
    @BindView(R.id.rv_slots)
    RecyclerView rvSlots;

    ArrayList<String> slotList = new ArrayList<>();
    SlotAdapter adapter;
    @BindView(R.id.text)
    AppCompatTextView text;
    @BindView(R.id.confirmSlot)
    AppCompatButton confirmSlot;
    @BindView(R.id.parentLay)
    RelativeLayout parentLay;
    private String textString;

    ApiInterface apiInterface;

    String slot_id, plcaeId, fee;

    SelfProgressUtils selfProgressUtils;


    ArrayList<AvailableSlots> availableSlots = new ArrayList<>();
    ArrayList<String> filledList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_grid_slot);
        ButterKnife.bind(this);
        setupToolBar();

        selfProgressUtils = new SelfProgressUtils(SelfGridSlotActivity.this);
        apiInterface = BaseRetrofit.getDefaultRetrofit().create(ApiInterface.class);

        if (getIntent() != null) {
            Intent intent = getIntent();
            textString = intent.getStringExtra("Address");
            plcaeId = intent.getStringExtra("PlaceId");
            fee = intent.getStringExtra("Fee");
        }

        if (textString != null) {
            text.setText("Your Slots For Selected Place is " + textString);
        }

        getAvailableSlot();
//        initRecyclerView();

        ItemClickSupport.addTo(rvSlots).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                Log.e("Nive ", "onItemClicked: ");
                adapter.OnItemClicked(position);
                adapter.notifyDataSetChanged();
                return false;
            }
        });

//
//        ItemClickSupport.addTo(rvSlots).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                Log.e("Nive ", "onItemClicked: ");
//                adapter.OnItemClicked(position);
//                adapter.notifyDataSetChanged();
//
//            }
//        });

    }

    private void getAvailableSlot() {

        if (App.getInstance().isOnline()) {
            selfProgressUtils.show();
            Call<AvailableSlotsResponse> availableSlotsResponseCall = apiInterface.getAvailable(plcaeId);
            availableSlotsResponseCall.enqueue(new Callback<AvailableSlotsResponse>() {
                @Override
                public void onResponse(Call<AvailableSlotsResponse> call, Response<AvailableSlotsResponse> response) {
                    selfProgressUtils.dismiss();
                    if (response.isSuccessful()) {

                        Log.e("Nive ", "onResponse: Available" + response);
                        addFiledList(response.body());
                    }
                }

                @Override
                public void onFailure(Call<AvailableSlotsResponse> call, Throwable t) {

                    selfProgressUtils.dismiss();

                }
            });
        }


    }

    private void addFiledList(AvailableSlotsResponse body) {

        for (int i = 0; i < body.getData().size(); i++) {
            if (i == 0) {

            } else {
                filledList.add(body.getData().get(i).getSlot_id());
            }

        }

        adapter = new SlotAdapter(this, slotList, filledList);
        adapter.setHasStableIds(true);
        rvSlots.setItemAnimator(null);
        rvSlots.setHasFixedSize(true);
        rvSlots.setLayoutManager(new GridLayoutManager(SelfGridSlotActivity.this, 4));

        rvSlots.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        Log.e("Nive ", "addFiledList: " + filledList);


    }

    private void initRecyclerView() {

        slotList.clear();

        for (int i = 0; i < 30; i++) {
            slotList.add(String.valueOf(i));

        }
        Log.e("ResponseArray", " : " + slotList);
        Log.e("Nive ", "initRecyclerView: " + filledList.size());
        adapter = new SlotAdapter(this, slotList, filledList);
        adapter.setHasStableIds(true);
        rvSlots.setItemAnimator(null);
        rvSlots.setHasFixedSize(true);
        rvSlots.setLayoutManager(new GridLayoutManager(SelfGridSlotActivity.this, 4));

        rvSlots.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    public void selectedId(String id) {

        Log.e("Nive ", "selectedId: " + id);
        this.slot_id = id;

    }

    private void setupToolBar() {


        toolbar.setNavigationIcon(R.drawable.back);


        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
    }


    @OnClick(R.id.confirmSlot)
    public void onViewClicked() {

        if (slot_id != null) {

            if (slot_id.isEmpty()) {
                BaseRetrofit.snack_bar(parentLay, "Please Select Slot!!! ", Snackbar.LENGTH_LONG, SelfGridSlotActivity.this);
//                Toast.makeText(GridSlotActivity.this, "", Toast.LENGTH_LONG).show();
            } else {
                if (App.getInstance().isOnline()) {

                    selfProgressUtils.show();

                    BookSlotRequest bookSlotRequest = new BookSlotRequest();
                    bookSlotRequest.setSlot_id(slot_id);
                    bookSlotRequest.setAmount(fee);
                    bookSlotRequest.setPlace_id(plcaeId);
                    bookSlotRequest.setUser_id(SelfPrefConnect.readString(SelfGridSlotActivity.this, "UserId", ""));

                    Call<BookSlotResponse> call = apiInterface.bookSlot(bookSlotRequest);
                    call.enqueue(new Callback<BookSlotResponse>() {
                        @Override
                        public void onResponse(Call<BookSlotResponse> call, Response<BookSlotResponse> response) {
                            selfProgressUtils.dismiss();
                            if (response.isSuccessful()) {
                                Log.e("Nive ", "onResponse: Book" + response);
                                showAlert(response);

                            }
                        }

                        @Override
                        public void onFailure(Call<BookSlotResponse> call, Throwable t) {
                            selfProgressUtils.dismiss();

                        }
                    });
                }

            }


        } else {

            BaseRetrofit.snack_bar(parentLay, "Please Select Slot!!! ", Snackbar.LENGTH_LONG, SelfGridSlotActivity.this);

//            Toast.makeText(GridSlotActivity.this, "Please Select Slot", Toast.LENGTH_LONG).show();
        }


    }

    public void selectedButton(String idenity) {

        if (idenity.equals("0")) {

            BaseRetrofit.snack_bar(parentLay, "This will be Filled, Please Select Next Slot!!!", Snackbar.LENGTH_LONG, SelfGridSlotActivity.this);

        } else {
            BaseRetrofit.snack_bar(parentLay, "This will be Selected, Please Press Confirm Button!!!", Snackbar.LENGTH_LONG, SelfGridSlotActivity.this);
        }

    }

    private void showAlert(final Response<BookSlotResponse> response) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Confirmed");
        builder.setMessage("Booking Has Been Confirmed Go to Payment");

        builder.setPositiveButton("Pay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                dialog.dismiss();

                Intent intent = new Intent(SelfGridSlotActivity.this, SelfPayActivity.class);
                intent.putExtra("BookConfirm", response.body());
                startActivity(intent);

            }
        });


        AlertDialog alert = builder.create();
        alert.show();
    }
}
