package com.android.vehicleparking.BaseRetrofit;

import android.app.Activity;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;


import com.android.vehicleparking.BuildConfig;
import com.android.vehicleparking.R;



import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRetrofit {


    public static String REQUEST = "REQUEST";
    public static String RESPONSE = "RESPONSE";
    public static String BASEURL = "http://35.154.6.47/self_parking/api/";
//    public static String BASEURL = "http://18.224.229.157/self_parking/api/";

    public static Retrofit retrofit = null;


    public static void snack_bar(View view, String mMessage, int mDuration, Activity activity) {
        Snackbar mSnackbar;
        mSnackbar = Snackbar.make(view, mMessage, mDuration);
        View mSnackbar_view = mSnackbar.getView();
        mSnackbar_view.setBackgroundColor(activity.getResources().getColor(R.color.snack_bar));
        TextView textView = (TextView) mSnackbar_view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(activity.getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mSnackbar.show();
    }


    /*public static Retrofit getAuthRetrofit() {


        LoggingInterceptor loggingInterceptor = new LoggingInterceptor.Builder()
                .addHeader("authId", "")
                .addHeader("authToken", "")
                .loggable(BuildConfig.DEBUG)
                .setLevel(HttpLoggingInterceptor.Level.BASIC)
                .request(REQUEST)
                .response(RESPONSE)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();

        retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(BASEURL).client(okHttpClient).build();

        return retrofit;


    }*/

    public static Retrofit getDefaultRetrofit() {


       /* LoggingInterceptor loggingInterceptor = new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(HttpLoggingInterceptor.Level.BASIC)
                .request(REQUEST)
                .response(RESPONSE)
                .build();*/

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

//        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(BASEURL).client(okHttpClient).build();

        return retrofit;


    }


}
