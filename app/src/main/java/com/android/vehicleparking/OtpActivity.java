package com.android.vehicleparking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.BaseClass.removeProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.showSimpleProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.snackbar;
import static com.android.vehicleparking.Common.ServiceClass.OTP_VERIFY;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_otp_code;
    TextView resend_otp_code, otp_number;
    Button btn_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        et_otp_code = (EditText) findViewById(R.id.et_otp_code);
        resend_otp_code = (TextView) findViewById(R.id.resend_otp_code);
        otp_number = (TextView) findViewById(R.id.otp_number);
        btn_otp = (Button) findViewById(R.id.btn_otp);

        btn_otp.setOnClickListener(this);
        otp_number.setText(getIntent().getStringExtra("mobile"));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_otp:
                if (TextUtils.isEmpty(et_otp_code.getText().toString())) {
                    Toast.makeText(this, "Enter your OTP", Toast.LENGTH_SHORT).show();
                } else {
                    otp_service();
                }
                break;

            case R.id.resend_otp_code:
                break;
        }
    }

    /**
     * Service call to verify the one time password
     */
    public void otp_service() {

        if (isOnline(this)) {

            showSimpleProgressDialog(this);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("otp", et_otp_code.getText().toString());
            params.put("mobile", otp_number.getText().toString());

            Log.i("Arun", "URL: " + OTP_VERIFY);
            Log.i("Arun", "PARAM: " + params);

            asyncHttpClient.post(OTP_VERIFY, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    removeProgressDialog();
                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        String message = jsonObject.optString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Toast.makeText(OtpActivity.this, message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(OtpActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            String error_message = jsonObject.optString("error_message");
                            Toast.makeText(OtpActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });
        } else {
            snackbar(this, btn_otp, NO_INTERNET);
        }
    }

}
