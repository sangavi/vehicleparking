package com.android.vehicleparking.app;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;


/**
 * Created by Admin on 2/27/2018.
 */

public class App extends Application {


    public static App mInstance;
    private Context context;

    public static App getInstance() {
        return mInstance;
    }

    public void offline() {
        Toast.makeText(context, "Please check your Internet connection", Toast.LENGTH_LONG).show();
    }

    public void onFailure() {

        Toast.makeText(context, "Slow internet connectionn", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        // do this once, for example in your Application class
        context = getApplicationContext();


    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

}
