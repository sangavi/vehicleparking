package com.android.vehicleparking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.Common.SessionManager;
import com.android.vehicleparking.SelfParking.SelfPrefConnect;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.BaseClass.removeProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.showSimpleProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.snackbar;
import static com.android.vehicleparking.Common.ServiceClass.REGISTER;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_username, et_email, et_mobile, et_password, et_cnfm_password;
    Button btn_register;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_username = (EditText) findViewById(R.id.et_username);
        et_email = (EditText) findViewById(R.id.et_email);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_password = (EditText) findViewById(R.id.et_password);
        et_cnfm_password = (EditText) findViewById(R.id.et_cnfm_password);
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);

        sessionManager = new SessionManager(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_register:
                if (checkvalidation()) {
                    register_service();
                }
                break;
        }
    }

    private Boolean checkvalidation() {
        if (TextUtils.isEmpty(et_username.getText().toString())) {
            et_username.setError("Enter your name");
            et_username.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(et_mobile.getText().toString())) {
            et_mobile.setError("Enter your mobile number");
            et_mobile.requestFocus();
            return false;
        } else if (et_mobile.getText().toString().length() < 10) {
            et_mobile.setError("Enter valid mobile number");
            et_mobile.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(et_email.getText().toString())) {
            et_email.setError("Enter email id");
            et_email.requestFocus();
            return false;
        } else if (!TextUtils.isEmpty(et_email.getText().toString())) {
            if (!BaseClass.eMailValidation(et_email.getText().toString())) {
                et_email.setError("Enter valid email");
                et_email.requestFocus();
                return false;
            }
        } else if (TextUtils.isEmpty(et_password.getText().toString()) || et_password.getText().toString().length() < 7) {
            et_password.setError("Password must be 6 character");
            et_password.requestFocus();
            return false;
        } else if (!et_cnfm_password.getText().toString().matches(et_password.getText().toString())) {
            et_cnfm_password.setError("Password not match");
            et_cnfm_password.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * Register Service Call after validating all the fields
     */
    public void register_service() {

        if (isOnline(this)) {

            showSimpleProgressDialog(this);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("name", et_username.getText().toString());
            params.put("mobile", et_mobile.getText().toString());
            params.put("email", et_email.getText().toString());
            params.put("password", et_cnfm_password.getText().toString());
            params.put("device_type", 1);
            params.put("device_token", BaseClass.device_token(this));

            Log.i("Arun", "URL: " + REGISTER);
            Log.i("Arun", "PARAM: " + params);

            asyncHttpClient.post(REGISTER, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    removeProgressDialog();
                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        String message = jsonObject.optString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();

                            JSONObject json_data = jsonObject.optJSONObject("data");
                            String id = json_data.optString("id");
                            String name = json_data.optString("name");
                            String mobile = json_data.optString("mobile");
                            String auth_token = json_data.optString("auth_token");
                            sessionManager.createLoginSession(id, auth_token, name);

                            SelfPrefConnect.writeString(RegisterActivity.this, "Register", mobile);
                            SelfPrefConnect.writeString(RegisterActivity.this, "UserId", id);

                            Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                            intent.putExtra("mobile", mobile);
                            startActivity(intent);

                        } else {
                            String error_message = jsonObject.optString("error_message");
                            Toast.makeText(RegisterActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });
        } else {
            snackbar(this, btn_register, NO_INTERNET);
        }
    }

}
