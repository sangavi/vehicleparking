package com.android.vehicleparking.Common;

public class ServiceClass {

    //Wallet Parking

    public static final String BASE_CLASS = "http://35.154.6.47/parking/api/customer/";
//    public static final String BASE_CLASS = "http://18.221.45.46/parking/api/customer/";

    public static final String REGISTER = BASE_CLASS + "register";
    public static final String LOGIN = BASE_CLASS + "login";
    public static final String OTP_VERIFY = BASE_CLASS + "otp_verify";
    public static final String NEARBY_LOCATION = BASE_CLASS + "nearby-locations";
    public static final String CREATE_PARKING = BASE_CLASS + "create-parking";
    public static final String REQUEST_STATUS_CHECK = BASE_CLASS + "request-status-check";
    public static final String STATUS_UPDATE = BASE_CLASS + "status-update";
    public static final String GET_PARKING_DETAILS = BASE_CLASS + "get-parking-details";
    public static final String GET_MY_CAR = BASE_CLASS + "get-my-car";
    public static final String HISTORY = BASE_CLASS + "history";
    public static final String GET_AMOUNT = BASE_CLASS + "get-amount";
    public static final String MAKE_PAYMENT = BASE_CLASS + "make-payment";

}
