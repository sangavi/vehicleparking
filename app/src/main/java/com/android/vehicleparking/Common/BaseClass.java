package com.android.vehicleparking.Common;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.vehicleparking.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;
import static com.android.vehicleparking.Common.SessionManager.KEY_ID;
import static com.android.vehicleparking.Common.SessionManager.KEY_TOKEN;
import static com.android.vehicleparking.Common.SessionManager.PREF_NAME;

public class BaseClass {

    public static Dialog mProgressDialog;
    public static final String NO_INTERNET = "Check your Internet Connection !";
    public static final String ACCESS_DENIED = "You are logged in from another device!";
    public static final String AUTHORIZATION = "Authorization";
    public static final TimeZone timeZone = TimeZone.getDefault();

    public static final int CHOOSE_PHOTO = 100;
    public static final int TAKE_PHOTO = 101;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static String access_token(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String access_token = sharedpreferences.getString(KEY_TOKEN, null);
        return access_token;
    }

    public static String device_token(Context context) {
        String device_token = FirebaseInstanceId.getInstance().getToken();
        return device_token;
    }

    public static String get_id(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String get_id = sharedpreferences.getString(KEY_ID, null);
        return get_id;
    }

    public static void logout_User(Context context) {
        SessionManager sessionManager = new SessionManager(context.getApplicationContext());
        sessionManager.logoutUser();
    }

    public static void snackbar(Context context, View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View sbview = snackbar.getView();
        sbview.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        TextView textView = (TextView) sbview.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public static void showSimpleProgressDialog(Context context) {
        if (context != null) {


            mProgressDialog = new Dialog(context, R.style.DialogThemeforview_pop);
            mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            mProgressDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setContentView(R.layout.animation_loading);
            mProgressDialog.show();
        }
    }

    public static void removeProgressDialog() {
        if (null != mProgressDialog)
            mProgressDialog.dismiss();
    }
}
