package com.android.vehicleparking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.Common.SessionManager;
import com.android.vehicleparking.SelfParking.SelfPrefConnect;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.BaseClass.removeProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.showSimpleProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.snackbar;
import static com.android.vehicleparking.Common.ServiceClass.LOGIN;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_user_name, et_password;
    Button btn_login;
    TextView new_user;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_user_name = (EditText) findViewById(R.id.et_user_name);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        new_user = (TextView) findViewById(R.id.new_user);

        sessionManager = new SessionManager(this);
        sessionManager.checkLogin();

        btn_login.setOnClickListener(this);
        new_user.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                if (checkvalidation()) {
                    login_service();
                }
                break;
            case R.id.new_user:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;

        }
    }

    private Boolean checkvalidation() {
        if (TextUtils.isEmpty(et_user_name.getText().toString())) {
            et_user_name.setError("Enter your mobile number");
            et_user_name.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(et_password.getText().toString())) {
            et_password.setError("Enter your name");
            et_password.requestFocus();
            return false;
        }
        return true;
    }


    /**
     * Service call for login
     */
    public void login_service() {

        if (isOnline(this)) {

            showSimpleProgressDialog(this);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("mobile", et_user_name.getText().toString());
            params.put("password", et_password.getText().toString());
            params.put("device_token", BaseClass.device_token(this));
            params.put("device_type", "1");

            Log.i("Arun", "URL: " + LOGIN);
            Log.i("Arun", "PARAM: " + params);

            asyncHttpClient.post(LOGIN, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    removeProgressDialog();
                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        String message = jsonObject.optString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                            JSONObject json_data = jsonObject.optJSONObject("data");
                            String id = json_data.optString("id");
                            String name = json_data.optString("name");
                            String mobile = json_data.optString("mobile");
                            String auth_token = json_data.optString("auth_token");
                            sessionManager.createLoginSession(id, auth_token, name);

                            SelfPrefConnect.writeString(LoginActivity.this, "Register", mobile);
                            SelfPrefConnect.writeString(LoginActivity.this, "UserId", id);

                            String otp_verified = json_data.optString("otp_verified");
                            if (otp_verified.equalsIgnoreCase("0")) {
                                Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                                intent.putExtra("mobile", mobile);
                                startActivity(intent);
                            } else if (otp_verified.equalsIgnoreCase("1")) {
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                            }

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(LoginActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    removeProgressDialog();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });
        } else {
            snackbar(this, btn_login, NO_INTERNET);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finishAffinity();
    }
}
