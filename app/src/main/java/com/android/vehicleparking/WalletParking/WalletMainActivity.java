package com.android.vehicleparking.WalletParking;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vehicleparking.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.skyfishjy.library.RippleBackground;
import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.Common.SessionManager;
import com.android.vehicleparking.Model.WalletNearByParkersPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.BaseClass.removeProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.snackbar;
import static com.android.vehicleparking.Common.ServiceClass.CREATE_PARKING;
import static com.android.vehicleparking.Common.ServiceClass.NEARBY_LOCATION;
import static com.android.vehicleparking.Common.ServiceClass.REQUEST_STATUS_CHECK;
import static com.android.vehicleparking.Common.ServiceClass.STATUS_UPDATE;

public class WalletMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener {


    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    SessionManager sessionManager;

    Button request_button;
    String location_id;
    String request_id;
    private Dialog req_load_dialog;

    Handler checkreqstatus;
    Double Latitude;
    Double Longitude;

    private ArrayList<WalletNearByParkersPojo> parkerslatlngs = new ArrayList<>();
    private ArrayList<Marker> markers = new ArrayList<Marker>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checkreqstatus = new Handler();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sessionManager = new SessionManager(this);

        request_button = findViewById(R.id.request_button);
        request_button.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int which) {
                            setResult(Activity.RESULT_CANCELED);
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.history) {
            Intent intent = new Intent(this, WalletHistoryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_about) {

        } else if (id == R.id.logout) {

            new AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int which) {
                            sessionManager.logoutUser();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        Latitude = location.getLatitude();
        Longitude = location.getLongitude();

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                16));

        get_latlang_serv(location.getLatitude(), location.getLongitude());

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.request_button:
                getDetailsAlert();
                break;
        }
    }

    /**
     * Service call to get the available parkers
     * Available parkers are mark into the map
     * @param lat
     * @param lang
     */
    public void get_latlang_serv(Double lat, Double lang) {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("lat", lat);
            params.put("lng", lang);

            Log.i("Arun", "URL: " + NEARBY_LOCATION);
            Log.i("Arun", "PARAM: " + params);

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(NEARBY_LOCATION, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {

                            JSONArray json_data = jsonObject.getJSONArray("data");
                            parkerslatlngs.clear();
                            for (int i = 0; i < json_data.length(); i++) {
                                JSONObject parkersObj = json_data.getJSONObject(i);
                                WalletNearByParkersPojo nearByParkers = new WalletNearByParkersPojo();
                                nearByParkers.setId(parkersObj.optString("id"));
                                nearByParkers.setLatlan(new LatLng(Double.valueOf(parkersObj.getString("lat")), Double.valueOf(parkersObj.getString("lng"))));
                                parkerslatlngs.add(nearByParkers);
                            }

                            if (parkerslatlngs.size() > 0) {

                                location_id = parkerslatlngs.get(0).getId();

                                if (null != markers && markers.size() > 0) {
                                    for (Marker marker : markers) {
                                        marker.remove();
                                    }
                                }
                                final Marker[] driver_marker = new Marker[1];
                                for (int i = 0; i < parkerslatlngs.size(); i++) {

                                    final MarkerOptions currentOption = new MarkerOptions();
                                    currentOption.position(parkerslatlngs.get(i).getLatlan());
//                                    currentOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                                    currentOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
                                    if (mMap != null) {
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                driver_marker[0] = mMap.addMarker(currentOption);

                                            }
                                        });
                                        markers.add(driver_marker[0]);
                                        request_button.setEnabled(true);
                                        request_button.setText("Request");
                                    }
                                }
                            } else {
                                request_button.setEnabled(true);
                                request_button.setText("No parking place available");
                            }

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletMainActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    removeProgressDialog();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });
        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Service call to create a parking request
     * @param vehicle_brand
     * @param vehicle_color
     * @param vehicle_number
     */
    public void create_parking(String vehicle_brand, String vehicle_color, String vehicle_number) {

        if (isOnline(this)) {

            showreqloader();

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("lat", Latitude);
            params.put("lng", Longitude);
            params.put("location_id", location_id);
            params.put("vehicle_brand", vehicle_brand);
            params.put("vehicle_color", vehicle_color);
            params.put("vehicle_number", vehicle_number);
            params.put("customer_id", BaseClass.get_id(this));

            Log.i("Arun", "URL: " + CREATE_PARKING);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(CREATE_PARKING, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {

                            JSONObject json_data = jsonObject.getJSONObject("data");
                            request_id = json_data.optString("id");
                            startCheckstatusTimer();

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletMainActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * To show the requesting popup while creating a request
     * popup will shown till the parker accept the request or admin reject the request
     */
    private void showreqloader() {

        req_load_dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        req_load_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        req_load_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_black)));
        req_load_dialog.setCancelable(false);
        req_load_dialog.setContentView(R.layout.request_loading);
        final RippleBackground rippleBackground = (RippleBackground) req_load_dialog.findViewById(R.id.content);
        Button cancel_req_create = req_load_dialog.findViewById(R.id.cancel_req_create);
        final TextView req_status = (TextView) req_load_dialog.findViewById(R.id.req_status);
        rippleBackground.startRippleAnimation();
        cancel_req_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_status.setText(getResources().getString(R.string.txt_canceling_req));
                cancel_request("99");
            }
        });


        req_load_dialog.show();
    }


    /**
     * To get the brand name , number , colour in these method
     * after getting above things , create a request
     */
    public void getDetailsAlert() {

        final Dialog dialog = new Dialog(this, R.style.DialogThemeforview);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.get_car_detail_popup);
        final EditText et_vehicle_brand = dialog.findViewById(R.id.et_vehicle_brand);
        final EditText et_vehicle_color = dialog.findViewById(R.id.et_vehicle_color);
        final EditText et_vehicle_number = dialog.findViewById(R.id.et_vehicle_number);
        final Button request_button_popup = dialog.findViewById(R.id.request_button_popup);
        final ImageButton popup_back = dialog.findViewById(R.id.popup_back);
        TextView dialog_ok_confir = (TextView) dialog.findViewById(R.id.dialog_ok_confir);
        TextView dialog_cancel_confir = (TextView) dialog.findViewById(R.id.dialog_cancel_confir);
        dialog.setCancelable(false);
        dialog.show();

        popup_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        request_button_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_vehicle_brand.getText().toString())) {
                    et_vehicle_brand.setError("Please Enter vehicle brand !");
//                    Toast.makeText(MainActivity.this, "Please Enter vehicle brand !", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_vehicle_color.getText().toString())) {
                    et_vehicle_color.setError("Please Enter vehicle color !");
//                    Toast.makeText(MainActivity.this, "Please Enter vehicle color  !", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_vehicle_number.getText().toString())) {
                    et_vehicle_number.setError("Please Enter vehicle number !");
//                    Toast.makeText(MainActivity.this, "Please Enter vehicle number  !", Toast.LENGTH_SHORT).show();
                } else {
                    if (isOnline(WalletMainActivity.this)) {
                        dialog.dismiss();
                        create_parking(et_vehicle_brand.getText().toString(), et_vehicle_color.getText().toString(), et_vehicle_number.getText().toString());
                    } else {
                        snackbar(WalletMainActivity.this, request_button, NO_INTERNET);
                    }
                }
            }
        });
    }

    Runnable reqrunnable = new Runnable() {
        public void run() {
            checkreqstatus();
            checkreqstatus.postDelayed(this, 4000);
        }
    };

    /**
     * To start the handler
     * This handler starts while creating a request
     * This will call four second's once
     */
    private void startCheckstatusTimer() {
        checkreqstatus.postDelayed(reqrunnable, 4000);
    }

    /**
     * Stop the handler
     * This method call while cancelling request or parker accept the request
     */
    private void stopCheckingforstatus() {
        if (checkreqstatus != null) {
            checkreqstatus.removeCallbacks(reqrunnable);

            Log.e("mahi", "stop status handler");
        }
    }


    /**
     * Method to check , weather the parker accept or not .
     * once the parker accepted , user redirect to next screen
     */
    private void checkreqstatus() {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));

            Log.e("Arun", "authId: " + BaseClass.get_id(this));
            Log.e("Arun", "authToken: " + BaseClass.access_token(this));
            Log.e("Arun", "REQUEST_STATUS_CHECK: " + REQUEST_STATUS_CHECK);

            asyncHttpClient.post(REQUEST_STATUS_CHECK, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        if (status.equalsIgnoreCase("true")) {
                            String request_status = jsonObject.optString("request_status");
                            if (request_status.equalsIgnoreCase("2")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                stopCheckingforstatus();
                                Intent intent = new Intent(WalletMainActivity.this, WalletTravelMapActivity.class);
                                intent.putExtra("request_id", request_id);
                                intent.putExtra("value", 1);
                                startActivity(intent);
                            } else if (request_status.equalsIgnoreCase("101")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                stopCheckingforstatus();
                                request_id = "";
                                startCheckstatusTimer();
                                Toast.makeText(WalletMainActivity.this, "Request cancelled by manager", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String error_message = jsonObject.optString("message");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Service call to cancel the requesting.
     * @param status
     */
    private void cancel_request(String status) {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);
            params.put("status", status);

            Log.i("Arun", "URL: " + STATUS_UPDATE);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(STATUS_UPDATE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {

                            if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                req_load_dialog.dismiss();
                            }

                            stopCheckingforstatus();
                            request_id = "";
                            startCheckstatusTimer();

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletMainActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

}

