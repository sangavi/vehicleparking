package com.android.vehicleparking.WalletParking;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.android.vehicleparking.Adapter.WalletHistoryAdapter;
import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.Model.WalletHistoryPojo;
import com.android.vehicleparking.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.BaseClass.removeProgressDialog;
import static com.android.vehicleparking.Common.BaseClass.showSimpleProgressDialog;
import static com.android.vehicleparking.Common.ServiceClass.HISTORY;

public class WalletHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv_history;
    TextView txt_no_history;
    ArrayList<WalletHistoryPojo> arrayList = new ArrayList<>();
    WalletHistoryAdapter walletHistoryAdapter;
    ImageView history_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);

        rv_history = findViewById(R.id.rv_history);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_history.setLayoutManager(mLayoutManager);
        rv_history.setItemAnimator(new DefaultItemAnimator());
        rv_history.setHasFixedSize(true);

        txt_no_history = findViewById(R.id.txt_no_history);
        history_back = findViewById(R.id.history_back);
        history_back.setOnClickListener(this);

        getHistory();

    }

    /**
     * Service call to get the history list
     */
    private void getHistory() {
        if (isOnline(this)) {

            showSimpleProgressDialog(this);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

            Log.i("Arun", "URL: " + HISTORY);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.get(HISTORY, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    removeProgressDialog();
                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        arrayList.clear();
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray jsonArray_data = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray_data.length(); i++) {

                                JSONObject json_data = jsonArray_data.optJSONObject(i);
                                WalletHistoryPojo walletHistoryPojo = new WalletHistoryPojo();
                                walletHistoryPojo.setId(json_data.optString("id"));
                                walletHistoryPojo.setLat(json_data.optString("lat"));
                                walletHistoryPojo.setLang(json_data.optString("lng"));
                                walletHistoryPojo.setVehicle_brand(json_data.optString("vehicle_brand"));
                                walletHistoryPojo.setVehicle_color(json_data.optString("vehicle_color"));
                                walletHistoryPojo.setVehicle_number(json_data.optString("vehicle_number"));
                                walletHistoryPojo.setTotal_time(json_data.optString("total_time"));
                                walletHistoryPojo.setAmount(json_data.optString("amount"));
                                walletHistoryPojo.setStatus(json_data.optString("status"));
                                walletHistoryPojo.setPayment_status(json_data.optString("payment_status"));

                                if (json_data.optString("id").equalsIgnoreCase("1")) {
                                    JSONObject json_parker = json_data.optJSONObject("parker");
                                    walletHistoryPojo.setParker_name(json_parker.optString("name"));
                                    walletHistoryPojo.setParker_mobile(json_parker.optString("mobile"));
                                }

                                String date1 = json_data.optString("parking_date");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date testDate = null;
                                try {
                                    testDate = sdf.parse(date1);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                SimpleDateFormat formatter_date = new SimpleDateFormat("MMM dd,yyyy");
                                String newFormat_date = formatter_date.format(testDate);
                                SimpleDateFormat formatter_time = new SimpleDateFormat("hh:mm a");
                                String newFormat_time = formatter_time.format(testDate);

                                walletHistoryPojo.setParking_date(newFormat_date);
                                walletHistoryPojo.setParking_time(newFormat_time);

                                arrayList.add(walletHistoryPojo);

                            }
                            if (arrayList.size() > 0) {
                                walletHistoryAdapter = new WalletHistoryAdapter(WalletHistoryActivity.this, arrayList);
                                rv_history.setAdapter(walletHistoryAdapter);
                            } else {
                                txt_no_history.setVisibility(View.VISIBLE);
                                rv_history.setVisibility(View.GONE);
                            }

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletHistoryActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    removeProgressDialog();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.history_back:
                finish();
                break;
        }
    }
}
