package com.android.vehicleparking.WalletParking;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.R;
import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.ServiceClass.GET_MY_CAR;
import static com.android.vehicleparking.Common.ServiceClass.REQUEST_STATUS_CHECK;
import static com.android.vehicleparking.Common.ServiceClass.STATUS_UPDATE;


public class WalletHistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBackDetail;
    HashMap<String, String> hashMap;
    private Dialog req_load_dialog;
    String request_id;
    Handler checkreqstatus_get;

    ImageView imgMap;
    Button btnReturnCar;
    TextView tv_vehicle_brand, tv_vehicle_number, tv_vehicle_colour, tv_parking_date, tv_parking_time, tv_total_amount, tv_total_time, tv_parker_name, tv_parker_mobile;
    LinearLayout Ll_parker_mobile, Ll_parker_name, Ll_total_time, Ll_total_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history_detail);

        checkreqstatus_get = new Handler();

        btnReturnCar = findViewById(R.id.btnReturnCar);
        imgBackDetail = findViewById(R.id.imgBackDetail);
        tv_vehicle_brand = findViewById(R.id.tv_vehicle_brand);
        tv_vehicle_colour = findViewById(R.id.tv_vehicle_colour);
        tv_vehicle_number = findViewById(R.id.tv_vehicle_number);
        tv_parking_date = findViewById(R.id.tv_parking_date);
        tv_parking_time = findViewById(R.id.tv_parking_time);
        tv_total_amount = findViewById(R.id.tv_total_amount);
        tv_total_time = findViewById(R.id.tv_total_time);
        tv_parker_name = findViewById(R.id.tv_parker_name);
        tv_parker_mobile = findViewById(R.id.tv_parker_mobile);
        imgMap = findViewById(R.id.imgMap);

        Ll_total_amount = findViewById(R.id.Ll_total_amount);
        Ll_total_time = findViewById(R.id.Ll_total_time);
        Ll_parker_name = findViewById(R.id.Ll_parker_name);
        Ll_parker_mobile = findViewById(R.id.Ll_parker_mobile);

        imgBackDetail.setOnClickListener(this);
        btnReturnCar.setOnClickListener(this);

        Intent intent = getIntent();
        hashMap = (HashMap<String, String>) intent.getSerializableExtra("HistoryListExtra");
        Log.e("Arun", "hashMap: " + hashMap);
        request_id = hashMap.get("id");

        String staticMapUrl = "http://maps.google.com/maps/api/staticmap?center=" + hashMap.get("lat") + "," + hashMap.get("lang") + "&markers=" + hashMap.get("lat") + "," + hashMap.get("lang") + "&zoom=16&size=270x270&sensor=false";
        Log.e("Arun", "staticMapUrl: " + staticMapUrl);
        Glide.with(this).load(staticMapUrl).into(imgMap);
        if (Integer.parseInt(hashMap.get("status")) < 4 || Integer.parseInt(hashMap.get("status")) == 99) {
            btnReturnCar.setVisibility(View.VISIBLE);
        }

        tv_vehicle_brand.setText(": " + hashMap.get("vehicle_brand"));
        tv_vehicle_colour.setText(": " + hashMap.get("vehicle_color"));
        tv_vehicle_number.setText(": " + hashMap.get("vehicle_number"));
        tv_parking_date.setText(": " + hashMap.get("parking_date"));
        tv_parking_time.setText(": " + hashMap.get("parking_time"));
        tv_total_time.setText(": " + hashMap.get("total_time"));
        tv_total_amount.setText(": " + hashMap.get("amount"));
        tv_parker_name.setText(": " + hashMap.get("parker_name"));
        tv_parker_mobile.setText(": " + hashMap.get("parker_mobile"));

        if (hashMap.get("payment_status").equalsIgnoreCase("2")) {
            Ll_total_time.setVisibility(View.VISIBLE);
            Ll_total_amount.setVisibility(View.VISIBLE);
            Ll_parker_name.setVisibility(View.VISIBLE);
            Ll_parker_mobile.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnReturnCar:
                get_my_car();
                break;

            case R.id.imgBackDetail:
                finish();
                break;
        }
    }

    Runnable reqrunnable_get = new Runnable() {
        public void run() {
            checkreqstatus();
            Log.e("Arun", "start checkreqstatus_get handler");
            checkreqstatus_get.postDelayed(this, 4000);
        }
    };

    /**
     * Start the request status Handler
     */
    private void startgetCheckstatusTimer() {
        checkreqstatus_get.postDelayed(reqrunnable_get, 4000);
    }

    /**
     * Stop the request status Handler
     */
    private void stopgetCheckingforstatus() {
        if (checkreqstatus_get != null) {
            checkreqstatus_get.removeCallbacks(reqrunnable_get);
            Log.e("Arun", "stop status handler");
        }
    }

    /**
     * Method for request the return car
     */
    private void get_my_car() {

        if (isOnline(this)) {

            showreqloader();

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            Log.i("Arun", "URL: " + GET_MY_CAR);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(GET_MY_CAR, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {
                            startgetCheckstatusTimer();
                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletHistoryDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    stopgetCheckingforstatus();
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * check the request status check
     * once the parker accept, redirect the user to next screen
     */
    private void checkreqstatus() {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));

            Log.e("Arun", "authId: " + BaseClass.get_id(this));
            Log.e("Arun", "authToken: " + BaseClass.access_token(this));
            Log.e("Arun", "REQUEST_STATUS_CHECK: " + REQUEST_STATUS_CHECK);

            asyncHttpClient.post(REQUEST_STATUS_CHECK, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        if (status.equalsIgnoreCase("true")) {
                            String request_status = jsonObject.optString("request_status");
                            if (request_status.equalsIgnoreCase("2")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                stopgetCheckingforstatus();
                                Intent intent = new Intent(WalletHistoryDetailActivity.this, WalletTravelMapActivity.class);
                                intent.putExtra("request_id", request_id);
                                intent.putExtra("value", 2);
                                startActivity(intent);
                                finish();
                            } else if (request_status.equalsIgnoreCase("101")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                Toast.makeText(WalletHistoryDetailActivity.this, "Request cancelled by manager", Toast.LENGTH_SHORT).show();
                                stopgetCheckingforstatus();
                            }
                        } else {
                            String error_message = jsonObject.optString("message");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    stopgetCheckingforstatus();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Request dialog method .
     * shown while requesting
     */
    private void showreqloader() {

        req_load_dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        req_load_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        req_load_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_black)));
        req_load_dialog.setCancelable(false);
        req_load_dialog.setContentView(R.layout.request_loading);
        final RippleBackground rippleBackground = (RippleBackground) req_load_dialog.findViewById(R.id.content);
        Button cancel_req_create = req_load_dialog.findViewById(R.id.cancel_req_create);
        final TextView req_status = (TextView) req_load_dialog.findViewById(R.id.req_status);
        rippleBackground.startRippleAnimation();
        cancel_req_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_status.setText(getResources().getString(R.string.txt_canceling_req));
                request_cancel("99");
            }
        });

        req_load_dialog.show();
    }

    /**
     * @param status Method for Cancel the return cap request
     */
    private void request_cancel(String status) {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);
            params.put("status", status);

            Log.i("Arun", "URL: " + STATUS_UPDATE);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(STATUS_UPDATE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {

                            if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                req_load_dialog.dismiss();
                            }
                            stopgetCheckingforstatus();
                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletHistoryDetailActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    stopgetCheckingforstatus();
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

}
