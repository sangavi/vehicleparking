package com.android.vehicleparking.WalletParking;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vehicleparking.Common.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.skyfishjy.library.RippleBackground;


import com.android.vehicleparking.Common.BaseClass;
import com.android.vehicleparking.R;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.android.vehicleparking.Common.BaseClass.NO_INTERNET;
import static com.android.vehicleparking.Common.BaseClass.isOnline;
import static com.android.vehicleparking.Common.ServiceClass.GET_AMOUNT;
import static com.android.vehicleparking.Common.ServiceClass.GET_MY_CAR;
import static com.android.vehicleparking.Common.ServiceClass.GET_PARKING_DETAILS;
import static com.android.vehicleparking.Common.ServiceClass.MAKE_PAYMENT;
import static com.android.vehicleparking.Common.ServiceClass.REQUEST_STATUS_CHECK;
import static com.android.vehicleparking.Common.ServiceClass.STATUS_UPDATE;

import cz.msebera.android.httpclient.Header;


public class WalletTravelMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    SessionManager sessionManager;

    Double Latitude;
    Double Longitude;
    Handler checkreqstatus;
    Handler checkreqstatus_get;
    String request_id;
    int value;

    TextView tv_parkername, tv_parker_phone, tv_status;
    LinearLayout Ll_status;
    Button return_car_button, confirm_payment;
    private Dialog req_load_dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_travel_map);

        sessionManager = new SessionManager(this);
        checkreqstatus = new Handler();
        checkreqstatus_get = new Handler();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_travel);
        mapFragment.getMapAsync(this);

        request_id = getIntent().getStringExtra("request_id");
        value = getIntent().getIntExtra("value", 0);

        tv_parkername = findViewById(R.id.tv_parkername);
        tv_parker_phone = findViewById(R.id.tv_parker_phone);
        tv_status = findViewById(R.id.tv_status);
        Ll_status = findViewById(R.id.Ll_status);
        return_car_button = findViewById(R.id.return_car_button);
        confirm_payment = findViewById(R.id.confirm_payment);
        return_car_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_my_car();
            }
        });

        if (value == 2) {
            return_car_button.setVisibility(View.GONE);
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        Latitude = location.getLatitude();
        Longitude = location.getLongitude();

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                16));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    Runnable reqrunnable = new Runnable() {
        public void run() {
            getParkingDetails();
            Log.e("Arun", "start status handler");
            checkreqstatus.postDelayed(this, 4000);
        }
    };

    /**
     * To start the handler
     * This handler starts to check the current status
     * This will call four second's once
     */
    private void startCheckstatusTimer() {
        checkreqstatus.postDelayed(reqrunnable, 4000);
    }

    /**
     * Stop the handler
     * This method call while user received the car after payment .
     */
    private void stopCheckingforstatus() {
        if (checkreqstatus != null) {
            checkreqstatus.removeCallbacks(reqrunnable);
            Log.e("Arun", "stop status handler");
        }
    }

    Runnable reqrunnable_get = new Runnable() {
        public void run() {
            checkreqstatus();
            Log.e("Arun", "start checkreqstatus_get handler");
            checkreqstatus_get.postDelayed(this, 4000);
        }
    };

    /**
     * To start the handler
     * This handler starts while creating a return vehicle request
     * This will call four second's once
     */
    private void startgetCheckstatusTimer() {
        checkreqstatus_get.postDelayed(reqrunnable_get, 4000);
    }


    /**
     * Stop the handler
     * This method call while cancelling request or parker accept the request
     */
    private void stopgetCheckingforstatus() {
        if (checkreqstatus_get != null) {
            checkreqstatus_get.removeCallbacks(reqrunnable_get);
            Log.e("Arun", "stop status handler");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCheckstatusTimer();
    }

    /**
     * To get the parking details after current location fetched
     */
    private void getParkingDetails() {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));

            Log.e("Arun", "authId: " + BaseClass.get_id(this));
            Log.e("Arun", "authToken: " + BaseClass.access_token(this));
            Log.e("Arun", "GET_PARKING_DETAILS: " + GET_PARKING_DETAILS);

            asyncHttpClient.post(GET_PARKING_DETAILS, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        if (status.equalsIgnoreCase("true")) {
                            JSONObject jsonObject1 = jsonObject.optJSONObject("parking_details");
                            String request_status = jsonObject1.optString("status");
                            LatLng latLng = new LatLng(Double.valueOf(jsonObject1.getString("lat")), Double.valueOf(jsonObject1.getString("lng")));

                            final MarkerOptions currentOption = new MarkerOptions();
                            currentOption.position(latLng);
//                            currentOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                            currentOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
                            mMap.addMarker(currentOption);

                            JSONObject jsonparker = jsonObject1.optJSONObject("parker");

                            String name = jsonparker.optString("name");
                            String mobile = jsonparker.optString("mobile");

                            tv_parkername.setText(name);
                            tv_parker_phone.setText(mobile);

                            if (request_status.equalsIgnoreCase("3")) {
                                return_car_button.setVisibility(View.VISIBLE);
                                Ll_status.setVisibility(View.VISIBLE);
                                tv_status.setText("Your vehicle is parked");
                            } else if (request_status.equalsIgnoreCase("5")) {
                                Ll_status.setVisibility(View.VISIBLE);
                                tv_status.setText("Parker picked your vehicle");
                            } else if (request_status.equalsIgnoreCase("6")) {
                                Ll_status.setVisibility(View.VISIBLE);
                                tv_status.setText("Make Payment");
                                get_amount();
                            } else if (request_status.equalsIgnoreCase("7")) {
                                stopCheckingforstatus();
                                Toast.makeText(WalletTravelMapActivity.this, "Your Payment confirmed", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(WalletTravelMapActivity.this, WalletMainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            String error_message = jsonObject.optString("message");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Service call to create a return vehicle request
     */
    private void get_my_car() {

        if (isOnline(this)) {

            showreqloader();

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            Log.i("Arun", "URL: " + GET_MY_CAR);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(GET_MY_CAR, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {
                            return_car_button.setVisibility(View.GONE);
                            stopCheckingforstatus();
                            startgetCheckstatusTimer();
                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletTravelMapActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    private void get_amount() {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            Log.i("Arun", "URL: " + GET_AMOUNT);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(GET_AMOUNT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {
                            String amount = jsonObject.optString("amount");
                            popup_amount(amount);
                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletTravelMapActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

    private void popup_amount(String amount) {
        new AlertDialog.Builder(this)
                .setMessage("Pay amount :" + amount)
                .setPositiveButton("Pay Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        make_payment();
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void make_payment() {

        if (isOnline(this)) {


            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            Log.i("Arun", "URL: " + MAKE_PAYMENT);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(MAKE_PAYMENT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {
                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletTravelMapActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Method to check , weather the parker accept or not return cap request.
     * once the parker accepted , other status are shown
     */
    private void checkreqstatus() {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));

            Log.e("Arun", "authId: " + BaseClass.get_id(this));
            Log.e("Arun", "authToken: " + BaseClass.access_token(this));
            Log.e("Arun", "REQUEST_STATUS_CHECK: " + REQUEST_STATUS_CHECK);

            asyncHttpClient.post(REQUEST_STATUS_CHECK, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");
                        if (status.equalsIgnoreCase("true")) {
                            String request_status = jsonObject.optString("request_status");
                            if (request_status.equalsIgnoreCase("2")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                stopgetCheckingforstatus();
                                startCheckstatusTimer();
                            } else if (request_status.equalsIgnoreCase("101")) {
                                if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                    req_load_dialog.dismiss();
                                }
                                Toast.makeText(WalletTravelMapActivity.this, "Request cancelled by manager", Toast.LENGTH_SHORT).show();
                                stopgetCheckingforstatus();
                                startCheckstatusTimer();
                            }
                        } else {
                            String error_message = jsonObject.optString("message");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    stopgetCheckingforstatus();
                    startCheckstatusTimer();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }
            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * To show the requesting popup while creating a return cap request
     * popup will shown till the parker accept the request or admin reject the request
     */
    private void showreqloader() {

        req_load_dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        req_load_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        req_load_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent_black)));
        req_load_dialog.setCancelable(false);
        req_load_dialog.setContentView(R.layout.request_loading);
        final RippleBackground rippleBackground = (RippleBackground) req_load_dialog.findViewById(R.id.content);
        Button cancel_req_create = req_load_dialog.findViewById(R.id.cancel_req_create);
        final TextView req_status = (TextView) req_load_dialog.findViewById(R.id.req_status);
        rippleBackground.startRippleAnimation();
        cancel_req_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_status.setText(getResources().getString(R.string.txt_canceling_req));
                request_cancel("99");
            }
        });

        req_load_dialog.show();
    }

    /**
     * Service call to cancel the return cap requesting.
     *
     * @param status
     */
    private void request_cancel(String status) {

        if (isOnline(this)) {

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("id", request_id);
            params.put("status", status);

            Log.i("Arun", "URL: " + STATUS_UPDATE);
            Log.i("Arun", "PARAM: " + params);
            Log.i("Arun", "authId: " + BaseClass.get_id(this));
            Log.i("Arun", "authToken: " + BaseClass.access_token(this));

            asyncHttpClient.addHeader("authId", BaseClass.get_id(this));
            asyncHttpClient.addHeader("authToken", BaseClass.access_token(this));
            asyncHttpClient.post(STATUS_UPDATE, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    String response = new String(responseBody);
                    Log.i("Arun", "onSuccess: " + response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.optString("status");

                        if (status.equalsIgnoreCase("true")) {

                            if (req_load_dialog != null && req_load_dialog.isShowing()) {
                                req_load_dialog.dismiss();
                            }

                            stopgetCheckingforstatus();
                            startCheckstatusTimer();

                        } else {
                            String error_message = jsonObject.optString("message");
                            Toast.makeText(WalletTravelMapActivity.this, error_message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    req_load_dialog.dismiss();
                    Log.i("Arun", "onFailure: " + error.getMessage());
                }

            });

        } else {
            Toast.makeText(this, NO_INTERNET, Toast.LENGTH_SHORT).show();
        }
    }

}

